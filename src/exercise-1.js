const getCommonItems = (array1, array2) => {
  // implement code here
  const result = [];
  for(const item1 of array1) {
    for(const item2 of array2) {
      if(item1 === item2) {
        result.push(item1);
      }
    }
  }
  return result;
};

export default getCommonItems;
