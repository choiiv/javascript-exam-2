const getTheExactNumber = (numbers) => {
  // implement code here
  return numbers.reduce((previous, current) => {
    if(current % 3 === 0) {
      if(previous === undefined || current > previous) {
        return current;
      }
    }
    return previous;
  }, undefined)
}

export default getTheExactNumber;